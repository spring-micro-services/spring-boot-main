package com.springboot.springbootmain.filter;

import com.netflix.zuul.ZuulFilter;
import org.springframework.stereotype.Component;

import java.util.logging.Logger;

/**
 * Created by ukomu01 on 25/02/16.
 */
public class LoggingFilter extends ZuulFilter {

  Logger log = Logger.getLogger(LoggingFilter.class.getName());

  @Override
  public String filterType() {
    return "pre";
  }

  @Override
  public int filterOrder() {
    return 0;
  }

  @Override
  public boolean shouldFilter() {
    return true;
  }

  @Override
  public Object run() {
    log.info("Zuul request received. Routing ...");
    return null;
  }

}
